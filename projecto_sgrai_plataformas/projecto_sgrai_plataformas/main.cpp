#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h> 
#include <math.h>
#include <time.h>
#include <vector>
#include <GL\freeglut.h>

#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"
#include "skybox.h"

#include <AL/alut.h>
//#pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"") 

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)
#define DEBUG               1
#define DELAY_MOVIMENTO     20

#define SPACEBAR 32
#define ESCAPE 27

#define GRAVIDADE 0.5
#define NCAMERAS 4

#define NUM_TEXTURAS 39
#define NOME_TEXTURA_PLAT_TOPO				"Texturas/plat_topo2.jpg"
#define NOME_TEXTURA_PLAT_TOPO2				"Texturas/plat_topo2.jpg"
#define NOME_TEXTURA_PLAT_LADO				"Texturas/plat_topo2.jpg"
#define NOME_TEXTURA_PLAT_LADO2				"Texturas/plat_lado2.jpg"
#define NOME_TEXTURA_PLAT_BASE				"Texturas/plat_topo2.jpg"
#define NOME_TEXTURA_PLAT_BASE2				"Texturas/plat_base2.jpg"
#define NOME_TEXTURA_PLAT_METAL				"Texturas/plat_metal.jpg"
#define NOME_TEXTURA_PLAT_WOOD				"Texturas/plat_wood.jpg"
#define NOME_TEXTURA_LAVA					"Texturas/lava2.jpg"
#define NOME_TEXTURA_BOLA_LAVA				"Texturas/fireBall7.jpg"
#define NOME_TEXTURA_NEW_GAME				"Texturas/new_game.jpg"
#define NOME_TEXTURA_MAIN_MENU_BG			"Texturas/main_menu_background.jpg"
#define NOME_TEXTURA_EXIT					"Texturas/exit.jpg"
#define NOME_TEXTURA_MULTI_GAME				"Texturas/multi_game.jpg"
#define NOME_TEXTURA_RESUME					"Texturas/resume.jpg"
#define NOME_TEXTURA_BACK					"Texturas/back.jpg"
#define NOME_TEXTURA_RESTART				"Texturas/restart.jpg"
#define NOME_TEXTURA_NEW_GAME_SELECTED		"Texturas/new_game_selected.jpg"
#define NOME_TEXTURA_EXIT_SELECTED			"Texturas/exit_selected.jpg"
#define NOME_TEXTURA_MULTI_GAME_SELECTED	"Texturas/multi_game_selected.jpg"
#define NOME_TEXTURA_RESUME_SELECTED		"Texturas/resume_selected.jpg"
#define NOME_TEXTURA_BACK_SELECTED			"Texturas/back_selected.jpg"
#define NOME_TEXTURA_RESTART_SELECTED		"Texturas/restart_selected.jpg"
#define NOME_TEXTURA_BONUS					"Texturas/box.jpg"
#define NOME_TEXTURA_BONUS_LADO				"Texturas/box_lado.jpg"
#define NOME_TEXTURA_PAUSE_MENU				"Texturas/pause_menu_background.jpg"
#define NOME_TEXTURA_INTRO					"Texturas/intro.jpg"
#define NOME_TEXTURA_BONUS_ICON1			"Texturas/bonus_icon1.jpg"
#define NOME_TEXTURA_BONUS_ICON2			"Texturas/bonus_icon2.jpg"
#define NOME_TEXTURA_YOU_WIN				"Texturas/you_win.jpg"
#define NOME_TEXTURA_OPTIONS_VOLUME_UP		"Texturas/options_volume_up.jpg"
#define NOME_TEXTURA_OPTIONS_VOLUME_UP_SELECTED	"Texturas/options_volume_up_selected.jpg"
#define NOME_TEXTURA_OPTIONS_VOLUME_DOWN		"Texturas/options_volume_down.jpg"
#define NOME_TEXTURA_OPTIONS_VOLUME_DOWN_SELECTED	"Texturas/options_volume_down_selected.jpg"
#define NOME_TEXTURA_OPTIONS_BACK			"Texturas/options_back.jpg"
#define NOME_TEXTURA_OPTIONS_BACK_SELECTED	"Texturas/options_back_selected.jpg"
#define NOME_TEXTURA_OPTIONS_BACKGROUND		"Texturas/options_menu_background.jpg"
#define NOME_TEXTURA_OPTIONS				"Texturas/options.jpg"
#define NOME_TEXTURA_OPTIONS_SELECTED		"Texturas/options_selected.jpg"





#define ID_TEXTURA_PLAT_TOPO 0
#define ID_TEXTURA_PLAT_TOPO2 1
#define ID_TEXTURA_PLAT_LADO 2
#define ID_TEXTURA_PLAT_LADO2 3
#define ID_TEXTURA_PLAT_BASE 4
#define ID_TEXTURA_PLAT_BASE2 5
#define ID_TEXTURA_PLAT_METAL 6
#define ID_TEXTURA_PLAT_WOOD 7
#define ID_TEXTURA_LAVA 8
#define ID_TEXTURA_BOLA_LAVA 9
#define ID_TEXTURA_NEW_GAME 10
#define ID_TEXTURA_MAIN_MENU_BG 11
#define ID_TEXTURA_EXIT 12
#define ID_TEXTURA_NEW_GAME_SELECTED 13
#define ID_TEXTURA_EXIT_SELECTED 14
#define ID_TEXTURA_MULTI_GAME 15
#define ID_TEXTURA_MULTI_GAME_SELECTED 16
#define ID_TEXTURA_BONUS 17
#define ID_TEXTURA_RESUME_SELECTED 18
#define ID_TEXTURA_RESUME 19
#define ID_TEXTURA_BACK_SELECTED 20
#define ID_TEXTURA_BACK 21
#define ID_TEXTURA_PAUSE_MENU 22
#define ID_TEXTURA_RESTART 23
#define ID_TEXTURA_RESTART_SELECTED 24
#define ID_TEXTURA_INTRO 25
#define ID_TEXTURA_BONUS_LADO 26
#define ID_TEXTURA_BONUS_ICON1 27
#define ID_TEXTURA_BONUS_ICON2 28
#define ID_TEXTURA_YOU_WIN 29
#define ID_TEXTURA_OPTIONS_VOLUME_UP 30
#define ID_TEXTURA_OPTIONS_VOLUME_UP_SELECTED 31
#define ID_TEXTURA_OPTIONS_VOLUME_DOWN 32
#define ID_TEXTURA_OPTIONS_VOLUME_DOWN_SELECTED 33
#define ID_TEXTURA_OPTIONS_BACK 34
#define ID_TEXTURA_OPTIONS_BACK_SELECTED 35
#define ID_TEXTURA_OPTIONS_BACKGROUND 36
#define ID_TEXTURA_OPTIONS 37
#define ID_TEXTURA_OPTIONS_SELECTED 38






#define BONUSTIMER 6000

typedef struct {				// OpenAL Struct
	ALuint	buffer, source;
} EstadoAL;

EstadoAL estadoAL_main;
EstadoAL estadoAL_jump;
EstadoAL estadoAL_doubleJump;
EstadoAL estadoAL_invincible;
EstadoAL estadoAL_options;
EstadoAL estadoAL_intro;
EstadoAL estadoAL_ending;

typedef struct {
	GLfloat     x, y, z;
	GLfloat		velocidade;
	GLfloat		min;
	GLfloat		max;
	GLfloat		dir;
	GLfloat		ballSize;
	GLfloat		vel;
} BolaDeFogo;

typedef struct {
	GLboolean   q, a, z, x, up, down, left, right, s, d, w, j, r, k, h, i, p, l;
}Teclas;

typedef struct {
	GLfloat    x, y, z;
}Pos;

typedef struct {
	Pos      eye, center, up;
	GLfloat  fov;
	GLfloat  angulo;
}Camera;

typedef struct {
	GLboolean   doubleBuffer;
	GLint       delayMovimento;
	Teclas      teclas;
	GLuint      menu_id;
	GLboolean   menuActivo;
	Camera      camera[2][NCAMERAS];
	GLint		cameraAtiva;
	GLboolean   debug;
	GLboolean   ortho;
	GLint	    multiPlayer;
}Estado;

typedef struct {
	GLfloat     x, y;
	GLint       pontuacao;
}Raquete;

typedef struct {
	GLfloat			x, y, z;
	GLfloat			velX, velY, velZ;
	GLboolean		onground;
	GLfloat			limSalto;
	GLfloat			incSalto;
	StudioModel		modelo;
	GLint			modeloCall;
	GLfloat			angulo;
	GLfloat			velocidade;
	GLboolean		aAndar;
	GLint			morreu;
	GLint			doublejump;
	GLint			jumps;
	GLfloat			sombra;
}Protagonista;

typedef struct {
	GLfloat     x, y, z;
	GLboolean	q_pisada;
	time_t		q_inicio;
	GLfloat		m_min;
	GLfloat		m_max;
	GLfloat		m_dir;
	GLboolean	m_pisada[2];
	GLboolean   protPisada;
	GLfloat		r, g, b;
	GLint		blink_timer;
	GLint		blink_reset;
	GLboolean	m_vert;
}Plataformas;

typedef struct {
	GLfloat     x, y, z;
	GLboolean	apanhado;
	GLfloat		tamanho;
	GLfloat		icon_alpha;
	GLfloat		icon_timer;
}Bonus;

typedef struct {
	BolaDeFogo		bolaDeFogo[30];
	Protagonista	protagonista[2];
	Bonus			bonus;
	Plataformas		plataformas_fixas[20];
	Plataformas		plataformas_movim[20];
	Plataformas		plataformas_queda[20];
	Plataformas		plataforma_fim;
	GLint			nfixas;
	GLint			nmovim;
	GLint			nqueda;
	GLboolean		parado;
	GLfloat			tamanho_plat;
	GLint			picos;
	GLint			activa;
	GLint			nBolas;
	GLint			ativarBonus;
}Modelo;

typedef struct {
	int sizeX, sizeY, bpp;
	char *data;
}JPGImage;

typedef struct {
	int width, height;
	bool hasAlpha;
	GLubyte *textureImage;
}PNGImage;

extern "C" int read_JPEG_file(char *, char **, int *, int *, int *);

GLint menuPrincipalOpcaoSelecionada = 0;

GLfloat	menuVectors[4][3] =
{
	{ -1.78,1,-1.72 },
	{ -1.78,-1,-1.72 },
	{ 1.78,-1,-1.72 },
	{ 1.78,1,-1.72 }
};
GLfloat mov = 0.1f;
GLfloat	menuButton[10][4][3] =
{
	{ { -0.70f,0.3f + mov,-0.9f }, //0
	{ -0.70f,0.175f + mov,-0.9f },
	{ -0.25f,0.175f + mov,-0.9f },
	{ -0.25f,0.3f + mov,-0.9f } },

	{ { -0.70f,0.075f + mov,-0.9f }, //1
	{ -0.70f,-0.05f + mov,-0.9f },
	{ -0.25f,-0.05f + mov,-0.9f },
	{ -0.25f,0.075f + mov,-0.9f } },

	{ { -0.70f,-0.15f + mov,-0.9f }, //2
	{ -0.70f,-0.275f + mov,-0.9f },
	{ -0.25f,-0.275f + mov,-0.9f },
	{ -0.25f,-0.15f + mov,-0.9f } },

	{ { -0.70f,-0.15f - 0.225f + mov,-0.9f }, //3
	{ -0.70f,-0.275f - 0.225f + mov,-0.9f },
	{ -0.25f,-0.275f - 0.225f + mov,-0.9f },
	{ -0.25f,-0.15f - 0.225f + mov,-0.9f }},

	{ { -0.70f,0.3f,-0.9f }, //4
	{ -0.70f,0.175f,-0.9f },
	{ -0.25f,0.175f,-0.9f },
	{ -0.25f,0.3f,-0.9f } },

	{ { -0.70f,0.075f,-0.9f }, //5
	{ -0.70f,-0.05f,-0.9f },
	{ -0.25f,-0.05f,-0.9f },
	{ -0.25f,0.075f,-0.9f } },

	{ { -0.70f,-0.15f,-0.9f }, //6
	{ -0.70f,-0.275f,-0.9f },
	{ -0.25f,-0.275f,-0.9f },
	{ -0.25f,-0.15f,-0.9f }},

	{ { -0.70f,0.3f,-0.9f }, //7
	{ -0.70f,0.175f,-0.9f },
	{ -0.25f,0.175f,-0.9f },
	{ -0.25f,0.3f,-0.9f } },

	{ { -0.70f,0.075f,-0.9f }, //8
	{ -0.70f,-0.05f,-0.9f },
	{ -0.25f,-0.05f,-0.9f },
	{ -0.25f,0.075f,-0.9f } },

	{ { -0.70f,-0.15f,-0.9f }, //9
	{ -0.70f,-0.275f,-0.9f },
	{ -0.25f,-0.275f,-0.9f },
	{ -0.25f,-0.15f,-0.9f } }
};

int player;
int WIDTH;
int HEIGHT;

float volume;
BolaDeFogo bolaDeFogo;
Estado estado;
Modelo modelo;
SKYBOX * skybox;
GLfloat raio = 2;
GLuint texVec[NUM_TEXTURAS];
GLfloat lavaX = 0, rotate = 0;

// Desenha bola de fogo
void desenhaBolaDeFogo(GLint tex) {

	for (int i = 0; i < modelo.nBolas; i++) {
		glEnable(GL_TEXTURE_2D);
		glPushMatrix();

		glTranslatef(modelo.bolaDeFogo[i].x, modelo.bolaDeFogo[i].y, modelo.bolaDeFogo[i].z);
		glRotatef(rotate, 1.0, 1.0, 1.0);

		glScalef(1.5, 1.5, 1.5);

		glBindTexture(GL_TEXTURE_2D, texVec[tex]);
		GLUquadricObj* sphere = gluNewQuadric();
		gluQuadricDrawStyle(sphere, GLU_FILL);
		gluQuadricNormals(sphere, GLU_SMOOTH);
		gluQuadricTexture(sphere, GL_TRUE);

		gluSphere(sphere, 1.0, 25, 25);

		gluDeleteQuadric(sphere);

		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}
}

/*------------------------Sound---------------------------*/
void InitAudioMainTheme() {
	estadoAL_main.buffer = alutCreateBufferFromFile("marble_zone.wav");
	alGenSources(1, &estadoAL_main.source);
	alSourcef(estadoAL_main.source, AL_GAIN, 0.3);
	alSourcei(estadoAL_main.source, AL_LOOPING, 1);						// Enables infinite loop
	alSourcef(estadoAL_main.source, AL_GAIN, volume);
	alSourcei(estadoAL_main.source, AL_BUFFER, estadoAL_main.buffer);
}

void InitAudioJump() {
	estadoAL_jump.buffer = alutCreateBufferFromFile("jump.wav");
	alGenSources(1, &estadoAL_jump.source);
	alSourcef(estadoAL_jump.source, AL_GAIN, 1);
	alSourcef(estadoAL_jump.source, AL_GAIN, volume);
	alSourcei(estadoAL_jump.source, AL_BUFFER, estadoAL_jump.buffer);

}

void InitAudioDoubleJump() {
	estadoAL_doubleJump.buffer = alutCreateBufferFromFile("TV.wav");
	alGenSources(1, &estadoAL_doubleJump.source);
	alSourcef(estadoAL_doubleJump.source, AL_GAIN, 1);
	alSourcef(estadoAL_doubleJump.source, AL_GAIN, volume);
	alSourcei(estadoAL_doubleJump.source, AL_BUFFER, estadoAL_doubleJump.buffer);

}

void InitAudioInvincible() {
	estadoAL_invincible.buffer = alutCreateBufferFromFile("invincible.wav");
	alGenSources(1, &estadoAL_invincible.source);
	alSourcef(estadoAL_invincible.source, AL_GAIN, 1);
	alSourcef(estadoAL_invincible.source, AL_GAIN, volume);
	alSourcei(estadoAL_invincible.source, AL_BUFFER, estadoAL_invincible.buffer);
}


void InitAudioOptions() {
	estadoAL_options.buffer = alutCreateBufferFromFile("options.wav");
	alGenSources(1, &estadoAL_options.source);
	alSourcef(estadoAL_options.source, AL_GAIN, 1);
	alSourcei(estadoAL_options.source, AL_LOOPING, 1);
	alSourcef(estadoAL_options.source, AL_GAIN, volume);
	alSourcei(estadoAL_options.source, AL_BUFFER, estadoAL_options.buffer);
}

void InitAudioIntro() {
	estadoAL_intro.buffer = alutCreateBufferFromFile("Sega.wav");
	alGenSources(1, &estadoAL_intro.source);
	alSourcef(estadoAL_intro.source, AL_GAIN, 1);
	alSourcef(estadoAL_intro.source, AL_GAIN, volume);
	alSourcei(estadoAL_intro.source, AL_BUFFER, estadoAL_intro.buffer);
}

void InitAudioEnding() {
	estadoAL_ending.buffer = alutCreateBufferFromFile("ending.wav");
	alGenSources(1, &estadoAL_ending.source);
	alSourcef(estadoAL_ending.source, AL_GAIN, 1);
	alSourcef(estadoAL_ending.source, AL_GAIN, volume);
	alSourcei(estadoAL_ending.source, AL_BUFFER, estadoAL_ending.buffer);
}

void initAllAudio() {
	InitAudioMainTheme();
	InitAudioDoubleJump();
	InitAudioJump();
	InitAudioInvincible();
	InitAudioOptions();
	InitAudioIntro();
	InitAudioEnding();
}

void changeVolume() {
	alSourcef(estadoAL_main.source, AL_GAIN, volume);
	alSourcef(estadoAL_jump.source, AL_GAIN, volume);
	alSourcef(estadoAL_doubleJump.source, AL_GAIN, volume);
	alSourcef(estadoAL_invincible.source, AL_GAIN, volume);
	alSourcef(estadoAL_options.source, AL_GAIN, volume);
	alSourcef(estadoAL_ending.source, AL_GAIN, volume);
}
/*------------------------Sound---------------------------*/

void inicia_modelo()
{
	modelo.protagonista[0].x = 0.0;
	modelo.protagonista[0].y = 0.0;
	modelo.protagonista[0].z = 15.0;
	modelo.protagonista[0].limSalto = 20;
	modelo.protagonista[0].incSalto = 0;
	modelo.protagonista[0].onground = GL_FALSE;
	modelo.protagonista[0].angulo = 270;
	modelo.protagonista[0].velocidade = 0.5;
	modelo.protagonista[0].doublejump = 0;
	modelo.protagonista[0].jumps = 0;
	modelo.protagonista[0].morreu = 0;
	modelo.protagonista[0].sombra = -15;

	modelo.protagonista[1].x = 0.0;
	modelo.protagonista[1].y = 0.0;
	modelo.protagonista[1].z = 10.0;
	modelo.protagonista[1].limSalto = 20;
	modelo.protagonista[1].incSalto = 0;
	modelo.protagonista[1].onground = GL_FALSE;
	modelo.protagonista[1].angulo = 270;
	modelo.protagonista[1].velocidade = 0.5;
	modelo.protagonista[1].doublejump = 0;
	modelo.protagonista[1].jumps = 0;
	modelo.protagonista[1].morreu = 0;
	modelo.protagonista[1].sombra = -15;

	modelo.tamanho_plat = 15.0;
}

void inicia_camera_fps() {
	estado.camera[0][0].eye.x = 0;
	estado.camera[0][0].eye.y = -0.5;
	estado.camera[0][0].eye.z = 0;
	estado.camera[0][0].center.x = 0;
	estado.camera[0][0].center.y = 0.5;
	estado.camera[0][0].center.z = 0;
	estado.camera[0][0].up.x = 0;
	estado.camera[0][0].up.y = 0;
	estado.camera[0][0].up.z = 1;
	estado.camera[0][0].fov = 60;
	estado.camera[0][0].angulo = 0;
	estado.camera[1][0].eye.x = 0;
	estado.camera[1][0].eye.y = -0.5;
	estado.camera[1][0].eye.z = 0;
	estado.camera[1][0].center.x = 0;
	estado.camera[1][0].center.y = 0.5;
	estado.camera[1][0].center.z = 0;
	estado.camera[1][0].up.x = 0;
	estado.camera[1][0].up.y = 0;
	estado.camera[1][0].up.z = 1;
	estado.camera[1][0].fov = 60;
	estado.camera[1][0].angulo = 0;
}

void inicia_camera_tps() {
	estado.camera[0][1].eye.x = 0;
	estado.camera[0][1].eye.y = 30;
	estado.camera[0][1].eye.z = 30;
	estado.camera[0][1].center.x = 0;
	estado.camera[0][1].center.y = 0;
	estado.camera[0][1].center.z = 0;;
	estado.camera[0][1].up.x = 0;
	estado.camera[0][1].up.y = 0;
	estado.camera[0][1].up.z = 1;
	estado.camera[0][1].fov = 60;
	estado.camera[0][1].angulo = 0;
	estado.camera[1][1].eye.x = 0;
	estado.camera[1][1].eye.y = 30;
	estado.camera[1][1].eye.z = 30;
	estado.camera[1][1].center.x = 0;
	estado.camera[1][1].center.y = 0;
	estado.camera[1][1].center.z = 0;;
	estado.camera[1][1].up.x = 0;
	estado.camera[1][1].up.y = 0;
	estado.camera[1][1].up.z = 1;
	estado.camera[1][1].fov = 60;
	estado.camera[1][1].angulo = 0;
}

void inicia_camera_top() {
	estado.camera[0][2].eye.x = 0;
	estado.camera[0][2].eye.y = 0;
	estado.camera[0][2].eye.z = 25;
	estado.camera[0][2].center.x = 0;
	estado.camera[0][2].center.y = 0;
	estado.camera[0][2].center.z = 0;
	estado.camera[0][2].up.x = 0;
	estado.camera[0][2].up.y = -1;
	estado.camera[0][2].up.z = 0;
	estado.camera[0][2].fov = 60;
	estado.camera[0][2].angulo = 0;
	estado.camera[1][2].eye.x = 0;
	estado.camera[1][2].eye.y = 0;
	estado.camera[1][2].eye.z = 25;
	estado.camera[1][2].center.x = 0;
	estado.camera[1][2].center.y = 0;
	estado.camera[1][2].center.z = 0;
	estado.camera[1][2].up.x = 0;
	estado.camera[1][2].up.y = -1;
	estado.camera[1][2].up.z = 0;
	estado.camera[1][2].fov = 60;
	estado.camera[1][2].angulo = 0;
}

void inicia_camera_fim(int prot) {
	estado.camera[0][3].eye.x = modelo.plataforma_fim.x;
	estado.camera[0][3].eye.y = modelo.plataforma_fim.y - 20;
	estado.camera[0][3].eye.z = modelo.protagonista[prot].z;
	estado.camera[0][3].center.x = modelo.protagonista[prot].x;
	estado.camera[0][3].center.y = modelo.protagonista[prot].y;
	estado.camera[0][3].center.z = modelo.protagonista[prot].z;
	estado.camera[0][3].up.x = 0;
	estado.camera[0][3].up.y = 0;
	estado.camera[0][3].up.z = 1;
	estado.camera[0][3].fov = 60;
	estado.camera[0][3].angulo = 0;
	estado.camera[1][3].eye.x = modelo.plataforma_fim.x;
	estado.camera[1][3].eye.y = modelo.plataforma_fim.y - 20;
	estado.camera[1][3].eye.z = modelo.protagonista[prot].z;
	estado.camera[1][3].center.x = modelo.protagonista[prot].x;
	estado.camera[1][3].center.y = modelo.protagonista[prot].y;
	estado.camera[1][3].center.z = modelo.protagonista[prot].z;
	estado.camera[1][3].up.x = 0;
	estado.camera[1][3].up.y = 0;
	estado.camera[1][3].up.z = 1;
	estado.camera[1][3].fov = 60;
	estado.camera[1][3].angulo = 0;
}

void inicia_bonus(GLfloat x, GLfloat y, GLfloat z, GLfloat tamanho) {
	modelo.bonus.x = x;
	modelo.bonus.y = y;
	modelo.bonus.z = z;
	modelo.bonus.apanhado = 0;
	modelo.bonus.tamanho = tamanho;
	modelo.bonus.icon_alpha = 1.0f;
	modelo.bonus.icon_timer = 2000;
}

void inicia_plataforma_especifica(char tipo[], GLfloat x, GLfloat y, GLfloat z, GLboolean vert) {

	if (strcmp(tipo, "queda") == 0) {
		if (modelo.nqueda < 20) {
			modelo.plataformas_queda[modelo.nqueda].x = x;
			modelo.plataformas_queda[modelo.nqueda].y = y;
			modelo.plataformas_queda[modelo.nqueda].z = z;

			modelo.plataformas_queda[modelo.nqueda].r = 0.5f;
			modelo.plataformas_queda[modelo.nqueda].g = 0.5f;
			modelo.plataformas_queda[modelo.nqueda].b = 0.5f;

			modelo.plataformas_queda[modelo.nqueda].q_pisada = GL_FALSE;
			modelo.plataformas_queda[modelo.nqueda].q_inicio = -1;

			modelo.plataformas_queda[modelo.nqueda].blink_timer = 1;
			modelo.plataformas_queda[modelo.nqueda].blink_reset = 1;
			modelo.nqueda++;
		}
	}
	if (strcmp(tipo, "movim") == 0) {
		if (modelo.nmovim < 20) {
			modelo.plataformas_movim[modelo.nmovim].x = x;
			modelo.plataformas_movim[modelo.nmovim].y = y;
			modelo.plataformas_movim[modelo.nmovim].z = z;

			modelo.plataformas_movim[modelo.nmovim].r = 0.5f;
			modelo.plataformas_movim[modelo.nmovim].g = 0.5f;
			modelo.plataformas_movim[modelo.nmovim].b = 0.5f;

			modelo.plataformas_movim[modelo.nmovim].q_pisada = GL_FALSE;
			modelo.plataformas_movim[modelo.nmovim].m_pisada[0] = GL_FALSE;
			modelo.plataformas_movim[modelo.nmovim].m_pisada[1] = GL_FALSE;


			modelo.plataformas_movim[modelo.nmovim].m_min = x - 10;
			modelo.plataformas_movim[modelo.nmovim].m_max = x + 10;
			if (vert) {
				modelo.plataformas_movim[modelo.nmovim].m_min = z - 15;
				modelo.plataformas_movim[modelo.nmovim].m_max = z + 10;
			}

			modelo.plataformas_movim[modelo.nmovim].m_dir = 0.25;
			modelo.plataformas_movim[modelo.nmovim].m_vert = vert;
			modelo.nmovim++;
		}
	}
	if (strcmp(tipo, "fixas") == 0) {
		if (modelo.nfixas < 20) {
			modelo.plataformas_fixas[modelo.nfixas].x = x;
			modelo.plataformas_fixas[modelo.nfixas].y = y;
			modelo.plataformas_fixas[modelo.nfixas].z = z;

			modelo.plataformas_fixas[modelo.nfixas].r = 0.5f;
			modelo.plataformas_fixas[modelo.nfixas].g = 0.5f;
			modelo.plataformas_fixas[modelo.nfixas].b = 0.5f;

			modelo.plataformas_fixas[modelo.nfixas].q_pisada = GL_FALSE;
			modelo.nfixas++;
		}
	}

	if (strcmp(tipo, "fim") == 0) {
		modelo.plataforma_fim.x = x;
		modelo.plataforma_fim.y = y;
		modelo.plataforma_fim.z = z;

		modelo.plataforma_fim.r = 0.5f;
		modelo.plataforma_fim.g = 0.5f;
		modelo.plataforma_fim.b = 0.5f;

		modelo.plataforma_fim.q_pisada = GL_FALSE;
	}

}

void inicia_todas_plataformas() {
	inicia_plataforma_especifica("fixas", 0.0, 0.0, 0.0, GL_FALSE);
	inicia_plataforma_especifica("fixas", 0.0, -20.0, 6.0, GL_FALSE);
	inicia_plataforma_especifica("fixas", 20.0, -40.0, 6.0, GL_FALSE);
	inicia_plataforma_especifica("fixas", 20.0, -60.0, 11.0, GL_FALSE);
	inicia_plataforma_especifica("movim", -10.0, -60.0, 11.0, GL_FALSE);

	inicia_plataforma_especifica("fixas", -40.0, -60.0, 11.0, GL_FALSE);
	inicia_plataforma_especifica("queda", -60.0, -60.0, 11.0, GL_FALSE);
	inicia_plataforma_especifica("queda", -60.0, -80.0, 16.0, GL_FALSE);

	inicia_plataforma_especifica("fixas", -40.0, -100.0, 16.0, GL_FALSE);
	// Bola
	inicia_plataforma_especifica("fixas", -40.0, -130.0, 16.0, GL_FALSE);
	// Bola
	inicia_plataforma_especifica("fixas", -40.0, -160.0, 16.0, GL_FALSE);

	inicia_plataforma_especifica("movim", -70.0, -160.0, 21.0, GL_FALSE);
	inicia_plataforma_especifica("fixas", -100.0, -160.0, 26.0, GL_FALSE);
	// Bola
	inicia_plataforma_especifica("queda", -100.0, -190.0, 26.0, GL_FALSE);
	// Bola
	inicia_plataforma_especifica("movim", -100.0, -220.0, 31.0, GL_TRUE);
	inicia_plataforma_especifica("queda", -80.0, -240.0, 31.0, GL_FALSE);
	// Bola
	inicia_plataforma_especifica("queda", -60.0, -260.0, 31.0, GL_FALSE);
	inicia_plataforma_especifica("fim", -60.0, -280.0, 36.0, GL_FALSE);
}


void inicia_BolaDeFogo(GLfloat x, GLfloat y, GLfloat z, GLfloat size) {
	if (modelo.nBolas < 30) {
		modelo.bolaDeFogo[modelo.nBolas].x = x;
		modelo.bolaDeFogo[modelo.nBolas].y = y;
		modelo.bolaDeFogo[modelo.nBolas].z = z;
		modelo.bolaDeFogo[modelo.nBolas].dir = 0;
		modelo.bolaDeFogo[modelo.nBolas].ballSize = size;
		modelo.bolaDeFogo[modelo.nBolas].vel = 0.5;
		modelo.nBolas++;
	}
}


// Inicia bolas de fogo
void iniciaBolasDeFogo() {
	inicia_BolaDeFogo(-40.0, -115.0, 70.0, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(-35.0, -115.0, 70.0, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(-45.0, -115.0, 70.0, bolaDeFogo.ballSize);

	inicia_BolaDeFogo(-40.0, -145.0, -22.0, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(-35.0, -145.0, -20.0, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(-45.0, -145.0, -20.0, bolaDeFogo.ballSize);

	inicia_BolaDeFogo(-100, -175, -25.0, bolaDeFogo.ballSize);

	inicia_BolaDeFogo(-100, -205, -30.0, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(-95, -205, -30.0, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(-105, -205, -30.0, bolaDeFogo.ballSize);

	inicia_BolaDeFogo(15, -28, -20, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(5, -32, -20, bolaDeFogo.ballSize);

	// Random balls
	inicia_BolaDeFogo(20, -75, -20, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(40, -300, -25, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(50, -120, -30, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(-400, -120, -20, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(-200, -300, -25, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(-160, -120, -30, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(-90, -90, -30, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(450, -60, -20, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(110, -90, -30, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(300, -200, -25, bolaDeFogo.ballSize);
	inicia_BolaDeFogo(230, -140, -30, bolaDeFogo.ballSize);
}

void setMaterial()
{
	GLfloat mat_specular[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat mat_shininess = 104;

	// cria��o autom�tica das componentes Ambiente e Difusa do material a partir das cores
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	// definir de outros parametros dos materiais est�ticamente
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}

void setLight()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	GLfloat light_pos[4] = { 0.0, 0.0, 10.0, 0.0 };
	GLfloat light_ambient[] = { 1.0f, 1.0f, 1.0f, 0.1f };
	GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 0.5f };
	GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 0.5f };

	// ligar iluminacao
	glEnable(GL_LIGHTING);

	//// ligar e definir fonte de luz 0
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, 0);
}

void shutdownLight() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glDisable(GL_LIGHTING);
	glDisable(GL_LIGHT0);
	glDisable(GL_COLOR_MATERIAL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_POINT_SMOOTH);
	glDisable(GL_LINE_SMOOTH);
	glDisable(GL_POLYGON_SMOOTH);
	glDisable(GL_NORMALIZE);
}

/****************************************
***  definição de texturas no vetor   ***
****************************************/
void defineTextures() {

	glGenTextures(NUM_TEXTURAS, texVec);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	JPGImage imagemJPG;
	PNGImage imagemPNG;
	if (read_JPEG_file(NOME_TEXTURA_PLAT_TOPO, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_PLAT_TOPO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_PLAT_TOPO);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_PLAT_TOPO2, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_PLAT_TOPO2]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_PLAT_TOPO2);
		exit(0);
	}


	if (read_JPEG_file(NOME_TEXTURA_PLAT_LADO, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_PLAT_LADO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_PLAT_LADO);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_PLAT_LADO2, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_PLAT_LADO2]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_PLAT_LADO2);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_PLAT_BASE, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_PLAT_BASE]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_PLAT_BASE);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_PLAT_BASE2, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_PLAT_BASE2]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_PLAT_BASE2);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_PLAT_METAL, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_PLAT_METAL]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_PLAT_METAL);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_PLAT_WOOD, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_PLAT_WOOD]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_PLAT_WOOD);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_LAVA, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_LAVA]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_LAVA);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_BOLA_LAVA, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_BOLA_LAVA]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_BOLA_LAVA);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_NEW_GAME, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_NEW_GAME]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_NEW_GAME);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_MAIN_MENU_BG, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_MAIN_MENU_BG]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_MAIN_MENU_BG);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_EXIT, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_EXIT]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_EXIT);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_NEW_GAME_SELECTED, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_NEW_GAME_SELECTED]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_NEW_GAME_SELECTED);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_EXIT_SELECTED, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_EXIT_SELECTED]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_EXIT_SELECTED);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_MULTI_GAME, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_MULTI_GAME]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_MULTI_GAME);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_MULTI_GAME_SELECTED, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_MULTI_GAME_SELECTED]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_MULTI_GAME_SELECTED);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_RESUME_SELECTED, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_RESUME_SELECTED]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_RESUME_SELECTED);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_RESUME, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_RESUME]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_RESUME);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_BACK, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_BACK]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_BACK);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_BACK_SELECTED, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_BACK_SELECTED]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_BACK_SELECTED);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_BONUS, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_BONUS]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_BONUS);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_PAUSE_MENU, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_PAUSE_MENU]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_PAUSE_MENU);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_RESTART, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_RESTART]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_RESTART);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_RESTART_SELECTED, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_RESTART_SELECTED]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_RESTART_SELECTED);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_INTRO, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_INTRO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_INTRO);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_BONUS_LADO, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_BONUS_LADO]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_BONUS_LADO);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_BONUS_ICON1, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_BONUS_ICON1]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_BONUS_ICON1);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_BONUS_ICON2, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_BONUS_ICON2]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_BONUS_ICON2);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_YOU_WIN, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_YOU_WIN]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_YOU_WIN);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_OPTIONS_VOLUME_UP, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_OPTIONS_VOLUME_UP]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_OPTIONS_VOLUME_UP);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_OPTIONS_VOLUME_UP_SELECTED, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_OPTIONS_VOLUME_UP_SELECTED]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_OPTIONS_VOLUME_UP_SELECTED);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_OPTIONS_VOLUME_DOWN, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_OPTIONS_VOLUME_DOWN]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_OPTIONS_VOLUME_DOWN);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_OPTIONS_VOLUME_DOWN_SELECTED, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_OPTIONS_VOLUME_DOWN_SELECTED]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_OPTIONS_VOLUME_DOWN_SELECTED);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_OPTIONS_BACK, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_OPTIONS_BACK]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_OPTIONS_BACK);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_OPTIONS_BACK_SELECTED, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_OPTIONS_BACK_SELECTED]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_OPTIONS_BACK_SELECTED);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_OPTIONS_BACKGROUND, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_OPTIONS_BACKGROUND]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_OPTIONS_BACKGROUND);
		exit(0);
	}

	if (read_JPEG_file(NOME_TEXTURA_OPTIONS, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_OPTIONS]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_OPTIONS);
		exit(0);
	}
	if (read_JPEG_file(NOME_TEXTURA_OPTIONS_SELECTED, &imagemJPG.data, &imagemJPG.sizeX, &imagemJPG.sizeY, &imagemJPG.bpp))
	{
		glBindTexture(GL_TEXTURE_2D, texVec[ID_TEXTURA_OPTIONS_SELECTED]);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		gluBuild2DMipmaps(GL_TEXTURE_2D, 3, imagemJPG.sizeX, imagemJPG.sizeY, GL_RGB, GL_UNSIGNED_BYTE, imagemJPG.data);
	}
	else {
		printf("Textura %s not Found\n", NOME_TEXTURA_OPTIONS_SELECTED);
		exit(0);
	}
	glBindTexture(GL_TEXTURE_2D, NULL);

}

void blinkColour(int id) {
	if (modelo.plataformas_queda[id].blink_reset == 0) {
		if (modelo.plataformas_queda[id].blink_timer == 1)
			modelo.plataformas_queda[id].blink_timer = 1000;
		if (modelo.plataformas_queda[id].r == 0.5f) {
			modelo.plataformas_queda[id].r = 1.0f;
			modelo.plataformas_queda[id].g = 0.0f;
			modelo.plataformas_queda[id].b = 0.0f;
		}
		else {
			modelo.plataformas_queda[id].r = 0.5f;
			modelo.plataformas_queda[id].g = 0.5f;
			modelo.plataformas_queda[id].b = 0.5f;
		}
		if (modelo.plataformas_queda[id].blink_timer > 125) {
			modelo.plataformas_queda[id].blink_timer = modelo.plataformas_queda[id].blink_timer / 2;
		}
		glutPostRedisplay();
		glutTimerFunc(modelo.plataformas_queda[id].blink_timer, blinkColour, id);
	}
	else {
		modelo.plataformas_queda[id].r = 0.5f;
		modelo.plataformas_queda[id].g = 0.5f;
		modelo.plataformas_queda[id].b = 0.5f;
	}
}

void disableDoubleJump(int prot) {
	alSourceStop(estadoAL_invincible.source);
	alSourcePlay(estadoAL_main.source);
	modelo.protagonista[prot].doublejump = 0;
}

void desenhaProtagonista(int id)
{
	glPushMatrix();
	glScalef(0.10, 0.10, 0.10);
	if (id == 0)
		mdlviewer_display(modelo.protagonista[id].modelo);
	else
		mdlviewer_display2(modelo.protagonista[id].modelo);
	glPopMatrix();
}

/* Inicialização do ambiente OPENGL */
void Init()
{
	/*----------------------------SOUND CODE------------------------------*/

	PlaySound(NULL, 0, 0);

	ALint state, state_jump, double_jump, invincible, options, intro, ending;
	alGetSourcei(estadoAL_main.source, AL_SOURCE_STATE, &state);	// Main theme sound
	alGetSourcei(estadoAL_jump.source, AL_SOURCE_STATE, &state_jump);	// Jump sound
	alGetSourcei(estadoAL_doubleJump.source, AL_SOURCE_STATE, &double_jump);	// double jump
	alGetSourcei(estadoAL_invincible.source, AL_SOURCE_STATE, &invincible); //Invincible
	alGetSourcei(estadoAL_options.source, AL_SOURCE_STATE, &options); //Options
	alGetSourcei(estadoAL_intro.source, AL_SOURCE_STATE, &intro); //Intro
	alGetSourcei(estadoAL_ending.source, AL_SOURCE_STATE, &ending); //Ending
	/*----------------------------SOUND CODE------------------------------*/

	srand((unsigned)time(NULL));

	glLoadIdentity();


	setLight();
	setMaterial();

	estado.cameraAtiva = 1;
	modelo.parado = GL_FALSE;

	estado.debug = 0;
	estado.delayMovimento = DELAY_MOVIMENTO;
	estado.ortho = GL_FALSE;

	modelo.nfixas = 0;
	modelo.nmovim = 0;
	modelo.nqueda = 0;

	modelo.nBolas = 0;
	bolaDeFogo.ballSize = 3.5f;

	modelo.protagonista[0].aAndar = GL_FALSE;
	modelo.protagonista[1].aAndar = GL_FALSE;

	inicia_camera_fps();
	inicia_camera_tps();
	inicia_camera_top();

	estado.teclas.a = estado.teclas.q = estado.teclas.z = estado.teclas.x = \
		estado.teclas.up = estado.teclas.down = estado.teclas.left = estado.teclas.right = estado.teclas.w = estado.teclas.s = estado.teclas.down = estado.teclas.j = estado.teclas.r = estado.teclas.i = estado.teclas.k = estado.teclas.l = estado.teclas.p = GL_FALSE;

	inicia_modelo();

	inicia_todas_plataformas();

	inicia_bonus(-10.0, -60.0, 28.5, 4.0);
	//inicias bolas de fogo
	iniciaBolasDeFogo();

	mdlviewer_init("sonic.mdl", modelo.protagonista[0].modelo);
	mdlviewer_init("tails.mdl", modelo.protagonista[1].modelo);

	/*----------------------------TEXTURE CODE------------------------------*/

	defineTextures();

	/*----------------------------TEXTURE CODE------------------------------*/

	//PlaySound(TEXT("main_theme"), NULL, SND_ASYNC | SND_LOOP);

}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, width, height);

	GLint c = estado.cameraAtiva;

	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(estado.camera[player][c].fov, (GLfloat)width / height, 0.6, 1000);

	// Matriz Modelview
	// Matriz onde são realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
}


void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLint text, Plataformas plat)
{
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, texVec[text]);
	glColor3f(plat.r, plat.g, plat.b);

	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex3fv(a);
	glTexCoord2f(0.0, 1.0);
	glVertex3fv(b);
	glTexCoord2f(1.0, 1.0);
	glVertex3fv(c);
	glTexCoord2f(1.0, 0.0);
	glVertex3fv(d);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, NULL);
	glDisable(GL_TEXTURE_2D);

}

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLint text)
{
	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, texVec[text]);
	glColor3f(0.5f, 0.5f, 0.5f);

	if (text == ID_TEXTURA_LAVA) {
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0 + lavaX, 0.0 + lavaX);
		glVertex3fv(a);
		glTexCoord2f(0.0 + lavaX, 1.0 + lavaX);
		glVertex3fv(b);
		glTexCoord2f(1.0 + lavaX, 1.0 + lavaX);
		glVertex3fv(c);
		glTexCoord2f(1.0 + lavaX, 0.0 + lavaX);
		glVertex3fv(d);
		glEnd();
	}
	else if (text == ID_TEXTURA_BOLA_LAVA) {
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0 + lavaX, 0.0 + lavaX);
		glVertex3fv(a);
		glTexCoord2f(0.0 + lavaX, 1.0 + lavaX);
		glVertex3fv(b);
		glTexCoord2f(1.0 + lavaX, 1.0 + lavaX);
		glVertex3fv(c);
		glTexCoord2f(1.0 + lavaX, 0.0 + lavaX);
		glVertex3fv(d);
		glEnd();
	}
	else {
		glBegin(GL_POLYGON);
		glTexCoord2f(0.0, 0.0);
		glVertex3fv(a);
		glTexCoord2f(0.0, 1.0);
		glVertex3fv(b);
		glTexCoord2f(1.0, 1.0);
		glVertex3fv(c);
		glTexCoord2f(1.0, 0.0);
		glVertex3fv(d);
		glEnd();
	}

	glBindTexture(GL_TEXTURE_2D, NULL);
	glDisable(GL_TEXTURE_2D);
}

void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[])
{
	glBegin(GL_POLYGON);
	glVertex3fv(a);
	glVertex3fv(b);
	glVertex3fv(c);
	glVertex3fv(d);
	glEnd();
}


void desenhaImagem(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLint text, GLfloat alpha)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D, texVec[text]);
	glColor4f(0.5f, 0.5f, 0.5f, alpha);

	glBegin(GL_POLYGON);
	glTexCoord2f(0.0, 0.0);
	glVertex2fv(a);
	glTexCoord2f(0.0, 1.0);
	glVertex2fv(b);
	glTexCoord2f(1.0, 1.0);
	glVertex2fv(c);
	glTexCoord2f(1.0, 0.0);
	glVertex2fv(d);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, NULL);
	glDisable(GL_TEXTURE_2D);

	glDisable(GL_BLEND);
}

//Desenha cubo bonus
void desenhaBonus(GLint topo, GLint base, GLint resto)
{
	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
	{ 0.5,-0.5,-0.5 },
	{ 0.5,0.5,-0.5 },
	{ -0.5,0.5,-0.5 },
	{ -0.5,-0.5,0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };


	desenhaPoligono(vertices[3], vertices[0], vertices[1], vertices[2], base);
	desenhaPoligono(vertices[7], vertices[3], vertices[2], vertices[6], resto);
	desenhaPoligono(vertices[4], vertices[0], vertices[3], vertices[7], resto);
	desenhaPoligono(vertices[6], vertices[2], vertices[1], vertices[5], resto);
	desenhaPoligono(vertices[6], vertices[7], vertices[4], vertices[5], topo);
	desenhaPoligono(vertices[5], vertices[1], vertices[0], vertices[4], resto);
}

//Desenha plataformas
void desenhaPlats(GLint topo, GLint base, GLint resto, Plataformas plat)
{
	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
	{ 0.5,-0.5,-0.5 },
	{ 0.5,0.5,-0.5 },
	{ -0.5,0.5,-0.5 },
	{ -0.5,-0.5,0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };


	desenhaPoligono(vertices[3], vertices[0], vertices[1], vertices[2], base, plat);
	desenhaPoligono(vertices[7], vertices[3], vertices[2], vertices[6], resto, plat);
	desenhaPoligono(vertices[4], vertices[0], vertices[3], vertices[7], resto, plat);
	desenhaPoligono(vertices[6], vertices[2], vertices[1], vertices[5], resto, plat);
	desenhaPoligono(vertices[6], vertices[7], vertices[4], vertices[5], topo, plat);
	desenhaPoligono(vertices[5], vertices[1], vertices[0], vertices[4], resto, plat);
}

void desenhaSombrasPlats() {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLfloat vertices[][3] = { { -0.5, -0.5, 0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };
	glColor4f(0, 0, 0, 0.5);
	desenhaPoligono(vertices[0], vertices[1], vertices[2], vertices[3]);
	glDisable(GL_BLEND);
}

void desenhaSombraProt() {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glColor4f(0, 0, 0, 0.5);
	glBegin(GL_POLYGON);
	for (double i = 0; i < 2 * M_PI; i += M_PI / 6)
		glVertex3f(cos(i) * raio, sin(i) * raio, 0.0);
	glEnd();
	glDisable(GL_BLEND);

}


void strokeString(char *str, double x, double y, double z, double s)
{
	int i, n;

	n = strlen(str);
	glPushMatrix();
	glColor3d(0.0, 1.0, 0.0);
	glTranslated(x, y, z);
	glScaled(s, s, s);
	for (i = 0; i < n; i++)
		glutStrokeCharacter(GLUT_STROKE_ROMAN, (int)str[i]);
	glPopMatrix();
}

void bitmapString(char *str, double x, double y)
{
	int i, n;

	n = strlen(str);
	glRasterPos2d(x, y);
	for (i = 0; i < n; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)str[i]);
}

void bitmapCenterString(char *str, double x, double y)
{
	int i, n;

	n = strlen(str);
	glRasterPos2d(x - glutBitmapLength(GLUT_BITMAP_HELVETICA_18, (const unsigned char *)str)*0.5, y);
	for (i = 0; i < n; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)str[i]);
}

void renderSpacedBitmapString(
	float x,
	float y,
	int spacing,
	char *string) {
	char *c;
	int x1 = x;

	for (c = string; *c != '\0'; c++) {
		glRasterPos2f(x1, y);
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, *c);
		x1 = x1 + glutBitmapWidth(GLUT_BITMAP_HELVETICA_18, *c) + spacing;
	}
}

void restorePerspectiveProjection() {
	glMatrixMode(GL_PROJECTION);
	// restore previous projection matrix
	glPopMatrix();
	// get back to modelview mode
	glMatrixMode(GL_MODELVIEW);
}

void setOrthographicProjection() {

	// switch to projection mode
	glMatrixMode(GL_PROJECTION);

	// save previous matrix which contains the
	//settings for the perspective projection
	glPushMatrix();

	// reset matrix
	glLoadIdentity();

	// set a 2D orthographic projection
	gluOrtho2D(0, WIDTH, HEIGHT, 0);

	// switch back to modelview mode
	glMatrixMode(GL_MODELVIEW);
}

void output() {
	setOrthographicProjection();
	glPushMatrix();
	glLoadIdentity();
	GLfloat vertices[][2] = {
	{ (GLfloat)(WIDTH / 2 - 291), (GLfloat)(((GLfloat)((GLfloat)HEIGHT / 2) - 81.5) - 150) },
	{ (GLfloat)(WIDTH / 2 - 291), (GLfloat)(((GLfloat)((GLfloat)HEIGHT / 2) + 81.5) - 150) },
	{ (GLfloat)(WIDTH / 2 + 291), (GLfloat)(((GLfloat)((GLfloat)HEIGHT / 2) + 81.5) - 150) },
	{ (GLfloat)(WIDTH / 2 + 291), (GLfloat)(((GLfloat)((GLfloat)HEIGHT / 2) - 81.5) - 150) } };
	desenhaImagem(vertices[0], vertices[1], vertices[2], vertices[3], ID_TEXTURA_YOU_WIN, 1.0);
	restorePerspectiveProjection();
	glPopMatrix();
}

void outputBonus(int control)
{
	if (control == 0) {
		modelo.bonus.icon_alpha = 1.0f;
		control = 1;
	}
	else {
		modelo.bonus.icon_alpha = 0.5f;
		control = 0;
	}
	modelo.bonus.icon_timer = modelo.bonus.icon_timer / 1.35f;

	if (modelo.protagonista[0].doublejump == 1 || modelo.protagonista[1].doublejump == 1) {
		glutTimerFunc(modelo.bonus.icon_timer, outputBonus, control);
	}
	else {
		modelo.bonus.icon_alpha = 0.0f;
	}
}

void DrawIntro(void) {
	desenhaPoligono(menuVectors[0], menuVectors[1], menuVectors[2], menuVectors[3], ID_TEXTURA_INTRO);
	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();
}

void DrawMenu(void)
{
	player = 0;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_NORMALIZE);

	glLoadIdentity();

	desenhaPoligono(menuVectors[0], menuVectors[1], menuVectors[2], menuVectors[3], ID_TEXTURA_MAIN_MENU_BG);
	//SinglePlayer
	if (menuPrincipalOpcaoSelecionada == 0)
		desenhaPoligono(menuButton[0][0], menuButton[0][1], menuButton[0][2], menuButton[0][3], ID_TEXTURA_NEW_GAME_SELECTED);
	else
		desenhaPoligono(menuButton[0][0], menuButton[0][1], menuButton[0][2], menuButton[0][3], ID_TEXTURA_NEW_GAME);
	//MultiPlayer
	if (menuPrincipalOpcaoSelecionada == 1)
		desenhaPoligono(menuButton[1][0], menuButton[1][1], menuButton[1][2], menuButton[1][3], ID_TEXTURA_MULTI_GAME_SELECTED);
	else
		desenhaPoligono(menuButton[1][0], menuButton[1][1], menuButton[1][2], menuButton[1][3], ID_TEXTURA_MULTI_GAME);
	//Quit
	if (menuPrincipalOpcaoSelecionada == 2)
		desenhaPoligono(menuButton[2][0], menuButton[2][1], menuButton[2][2], menuButton[2][3], ID_TEXTURA_OPTIONS_SELECTED);
	else
		desenhaPoligono(menuButton[2][0], menuButton[2][1], menuButton[2][2], menuButton[2][3], ID_TEXTURA_OPTIONS);

	if (menuPrincipalOpcaoSelecionada == 3)
		desenhaPoligono(menuButton[3][0], menuButton[3][1], menuButton[3][2], menuButton[3][3], ID_TEXTURA_EXIT_SELECTED);
	else
		desenhaPoligono(menuButton[3][0], menuButton[3][1], menuButton[3][2], menuButton[3][3], ID_TEXTURA_EXIT);

	glFlush();

	if (estado.doubleBuffer)
		glutSwapBuffers();

}

void DrawMenu2(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_NORMALIZE);

	glLoadIdentity();

	player = 0;

	desenhaPoligono(menuVectors[0], menuVectors[1], menuVectors[2], menuVectors[3], ID_TEXTURA_PAUSE_MENU);
	//Resume
	if (menuPrincipalOpcaoSelecionada == 4)
		desenhaPoligono(menuButton[4][0], menuButton[4][1], menuButton[4][2], menuButton[4][3], ID_TEXTURA_RESUME_SELECTED);
	else
		desenhaPoligono(menuButton[4][0], menuButton[4][1], menuButton[4][2], menuButton[4][3], ID_TEXTURA_RESUME);
	//Restart
	if (menuPrincipalOpcaoSelecionada == 5)
		desenhaPoligono(menuButton[5][0], menuButton[5][1], menuButton[5][2], menuButton[5][3], ID_TEXTURA_RESTART_SELECTED);
	else
		desenhaPoligono(menuButton[5][0], menuButton[5][1], menuButton[5][2], menuButton[5][3], ID_TEXTURA_RESTART);
	//Back
	if (menuPrincipalOpcaoSelecionada == 6)
		desenhaPoligono(menuButton[6][0], menuButton[6][1], menuButton[6][2], menuButton[6][3], ID_TEXTURA_BACK_SELECTED);
	else
		desenhaPoligono(menuButton[6][0], menuButton[6][1], menuButton[6][2], menuButton[6][3], ID_TEXTURA_BACK);

	glFlush();

	if (estado.doubleBuffer)
		glutSwapBuffers();

}

void DrawMenu3(void)
{
	player = 0;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_NORMALIZE);

	glLoadIdentity();

	desenhaPoligono(menuVectors[0], menuVectors[1], menuVectors[2], menuVectors[3], ID_TEXTURA_OPTIONS_BACKGROUND);
	//SinglePlayer
	if (menuPrincipalOpcaoSelecionada == 7)
		desenhaPoligono(menuButton[7][0], menuButton[7][1], menuButton[7][2], menuButton[7][3], ID_TEXTURA_OPTIONS_VOLUME_UP_SELECTED);
	else
		desenhaPoligono(menuButton[7][0], menuButton[7][1], menuButton[7][2], menuButton[7][3], ID_TEXTURA_OPTIONS_VOLUME_UP);
	//MultiPlayer
	if (menuPrincipalOpcaoSelecionada == 8)
		desenhaPoligono(menuButton[8][0], menuButton[8][1], menuButton[8][2], menuButton[8][3], ID_TEXTURA_OPTIONS_VOLUME_DOWN_SELECTED);
	else
		desenhaPoligono(menuButton[8][0], menuButton[8][1], menuButton[8][2], menuButton[8][3], ID_TEXTURA_OPTIONS_VOLUME_DOWN);
	//Quit
	if (menuPrincipalOpcaoSelecionada == 9)
		desenhaPoligono(menuButton[9][0], menuButton[9][1], menuButton[9][2], menuButton[9][3], ID_TEXTURA_OPTIONS_BACK_SELECTED);
	else
		desenhaPoligono(menuButton[9][0], menuButton[9][1], menuButton[9][2], menuButton[9][3], ID_TEXTURA_OPTIONS_BACK);

	glFlush();

	if (estado.doubleBuffer)
		glutSwapBuffers();

}

void Draw()
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_NORMALIZE);

	glLoadIdentity();

	GLint c = estado.cameraAtiva;

	if (c == 0) {
		glTranslatef(0, 0, 1.0);
		glRotatef(estado.camera[player][c].angulo, 0.0, 1.0, 0.0);
	}

	if (c == 1) {
		glTranslatef(0, 0.0, -30.0);
		glRotatef(15.0, 1.0, 0.0, 0.0);
		glRotatef(estado.camera[player][c].angulo, 0.0, 1.0, 0.0);
		glTranslatef(0, -5.0, 30.0);
	}

	if (c == 2) {
		glRotatef(estado.camera[player][c].angulo, 0.0, 0.0, 1.0);
	}

	gluLookAt(estado.camera[player][c].eye.x, estado.camera[player][c].eye.y, estado.camera[player][c].eye.z, \
		estado.camera[player][c].center.x, estado.camera[player][c].center.y, estado.camera[player][c].center.z, \
		estado.camera[player][c].up.x, estado.camera[player][c].up.y, estado.camera[player][c].up.z);

	glTranslatef(0, 0, 0);

	skybox->Render(estado.camera[player][c].angulo, 0);

	GLfloat v0_chao[3] = { -500,500,-15 };
	GLfloat v1_chao[3] = { -500,-500,-15 };
	GLfloat v2_chao[3] = { 500,-500,-15 };
	GLfloat v3_chao[3] = { 500,500,-15 };

	glPushMatrix();
	desenhaPoligono(v0_chao, v1_chao, v2_chao, v3_chao, ID_TEXTURA_LAVA);
	glPopMatrix();

	//Draw bolas de fogo
	desenhaBolaDeFogo(ID_TEXTURA_BOLA_LAVA);
	glNormal3f(0, 0, 1);

	if (c != 0) {

		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glTranslatef(modelo.protagonista[0].x, modelo.protagonista[0].y, modelo.protagonista[0].z);
		glRotatef(modelo.protagonista[0].angulo, 0.0, 0.0, 1.0);
		desenhaProtagonista(0);
		glDisable(GL_TEXTURE_2D);
		glPopMatrix();
		if (estado.multiPlayer == 1) {
			glPushMatrix();
			glEnable(GL_TEXTURE_2D);
			glTranslatef(modelo.protagonista[1].x, modelo.protagonista[1].y, modelo.protagonista[1].z);
			glRotatef(modelo.protagonista[1].angulo, 0.0, 0.0, 1.0);
			desenhaProtagonista(1);
			glDisable(GL_TEXTURE_2D);
			glPopMatrix();
		}
	}

	if (!modelo.activa) {
		output();
	}

	if (modelo.protagonista[0].doublejump == 1 || modelo.protagonista[1].doublejump == 1) {
		setOrthographicProjection();
		glPushMatrix();
		glLoadIdentity();
		GLfloat vertices[][2] = { { WIDTH - (WIDTH - 25), HEIGHT - (HEIGHT - 25) },
		{ WIDTH - (WIDTH - 25), HEIGHT - (HEIGHT - 75) },
		{ WIDTH - (WIDTH - 75), HEIGHT - (HEIGHT - 75) },
		{ WIDTH - (WIDTH - 75), HEIGHT - (HEIGHT - 25) } };
		if (estado.multiPlayer == 1) {
			vertices[2][0] = (WIDTH - (WIDTH - 125));
			vertices[3][0] = ((WIDTH - (WIDTH - 125)));
		}
		if (modelo.protagonista[0].doublejump == 1) {
			desenhaImagem(vertices[0], vertices[1], vertices[2], vertices[3], ID_TEXTURA_BONUS_ICON1, modelo.bonus.icon_alpha);
		}
		else {
			desenhaImagem(vertices[0], vertices[1], vertices[2], vertices[3], ID_TEXTURA_BONUS_ICON2, modelo.bonus.icon_alpha);
		}

		restorePerspectiveProjection();
		glPopMatrix();

		if (modelo.ativarBonus == 1) {
			outputBonus(0);
			modelo.ativarBonus = 0;
		}
	}

	for (int a = 0; a < modelo.nfixas; a++) {
		glPushMatrix();
		glTranslatef(modelo.plataformas_fixas[a].x, modelo.plataformas_fixas[a].y, modelo.plataformas_fixas[a].z);
		glScalef(modelo.tamanho_plat, modelo.tamanho_plat, 2.0);
		desenhaPlats(ID_TEXTURA_PLAT_TOPO, ID_TEXTURA_PLAT_BASE, ID_TEXTURA_PLAT_LADO, modelo.plataformas_fixas[a]);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(modelo.plataformas_fixas[a].x, modelo.plataformas_fixas[a].y, -14.5);
		glScalef(modelo.tamanho_plat, modelo.tamanho_plat, 1.0);
		desenhaSombrasPlats();
		glPopMatrix();
	}

	for (int a = 0; a < modelo.nqueda; a++) {
		glPushMatrix();
		glTranslatef(modelo.plataformas_queda[a].x, modelo.plataformas_queda[a].y, modelo.plataformas_queda[a].z);
		glScalef(modelo.tamanho_plat, modelo.tamanho_plat, 2.0);
		desenhaPlats(ID_TEXTURA_PLAT_TOPO2, ID_TEXTURA_PLAT_BASE2, ID_TEXTURA_PLAT_LADO2, modelo.plataformas_queda[a]);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(modelo.plataformas_queda[a].x, modelo.plataformas_queda[a].y, -14.5);
		glScalef(modelo.tamanho_plat, modelo.tamanho_plat, 1.0);
		desenhaSombrasPlats();
		glPopMatrix();
	}

	for (int a = 0; a < modelo.nmovim; a++) {
		glPushMatrix();
		glTranslatef(modelo.plataformas_movim[a].x, modelo.plataformas_movim[a].y, modelo.plataformas_movim[a].z);
		glScalef(modelo.tamanho_plat, modelo.tamanho_plat, 2.0);
		desenhaPlats(ID_TEXTURA_PLAT_WOOD, ID_TEXTURA_PLAT_WOOD, ID_TEXTURA_PLAT_WOOD, modelo.plataformas_movim[a]);
		glPopMatrix();

		glPushMatrix();
		glTranslatef(modelo.plataformas_movim[a].x, modelo.plataformas_movim[a].y, -14.5);
		glScalef(modelo.tamanho_plat, modelo.tamanho_plat, 1.0);
		desenhaSombrasPlats();
		glPopMatrix();
	}

	glPushMatrix();
	glTranslatef(modelo.plataforma_fim.x, modelo.plataforma_fim.y, modelo.plataforma_fim.z);
	glScalef(modelo.tamanho_plat, modelo.tamanho_plat, 2.0);
	desenhaPlats(ID_TEXTURA_PLAT_METAL, ID_TEXTURA_PLAT_METAL, ID_TEXTURA_PLAT_METAL, modelo.plataforma_fim);
	glPopMatrix();


	glPushMatrix();
	glTranslatef(modelo.plataforma_fim.x, modelo.plataforma_fim.y, -14.5);
	glScalef(modelo.tamanho_plat, modelo.tamanho_plat, 1.0);
	desenhaSombrasPlats();
	glPopMatrix();

	if (modelo.bonus.apanhado == 0) {
		glPushMatrix();
		glTranslatef(modelo.bonus.x, modelo.bonus.y, modelo.bonus.z);
		glScalef(modelo.bonus.tamanho, modelo.bonus.tamanho, modelo.bonus.tamanho);
		desenhaBonus(ID_TEXTURA_BONUS, ID_TEXTURA_BONUS, ID_TEXTURA_BONUS_LADO);
		glPopMatrix();
	}

	glPushMatrix();
	glTranslatef(modelo.protagonista[0].x, modelo.protagonista[0].y, modelo.protagonista[0].sombra);
	desenhaSombraProt();
	glPopMatrix();
	if (estado.multiPlayer == 1) {
		glPushMatrix();
		glTranslatef(modelo.protagonista[1].x, modelo.protagonista[1].y, modelo.protagonista[1].sombra);
		desenhaSombraProt();
		glPopMatrix();
	}

	glFlush();

	if (estado.doubleBuffer)
		glutSwapBuffers();
}


void DisplayMultiplayer() {
	player = 0;
	glEnable(GL_SCISSOR_TEST);
	glViewport(0, 0, WIDTH / 2, HEIGHT);
	glScissor(0, 0, WIDTH / 2, HEIGHT);
	Draw();

	player = 1;
	glViewport(WIDTH / 2, 0, WIDTH / 2, HEIGHT);
	glScissor(WIDTH / 2, 0, WIDTH / 2, HEIGHT);
	Draw();

	glDisable(GL_SCISSOR_TEST);

	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();
}

int testaColisaoTodos(int prot) {

	modelo.protagonista[prot].sombra = -14.5;

	for (int i = 0; i < modelo.nqueda; i++) {
		if (modelo.protagonista[prot].x > (modelo.plataformas_queda[i].x - (float)modelo.tamanho_plat / 2) && modelo.protagonista[prot].x < (modelo.plataformas_queda[i].x + (float)modelo.tamanho_plat / 2) && modelo.protagonista[prot].y < (modelo.plataformas_queda[i].y + (float)modelo.tamanho_plat / 2) && modelo.protagonista[prot].y >(modelo.plataformas_queda[i].y - (float)modelo.tamanho_plat / 2)) {
			if (modelo.protagonista[prot].z > modelo.plataformas_queda[i].z) {
				modelo.protagonista[prot].sombra = modelo.plataformas_queda[i].z + 1.5;
			}

			if (modelo.protagonista[prot].z - 4 < (modelo.plataformas_queda[i].z + 2) && modelo.protagonista[prot].z - 4 > (modelo.plataformas_queda[i].z - 0.5)) {

				//Blink red
				if (modelo.plataformas_queda[i].q_pisada == GL_FALSE) {
					modelo.plataformas_queda[i].blink_reset = 0;
					glutTimerFunc(modelo.plataformas_queda[i].blink_timer, blinkColour, i);
				}

				modelo.plataformas_queda[i].q_pisada = GL_TRUE;
				modelo.protagonista[prot].incSalto = 0;

				if (modelo.protagonista[prot].modelo.GetSequence() == 8)
					modelo.protagonista[prot].modelo.SetSequence(0);
				if (modelo.plataformas_queda[i].q_inicio == -1)
					modelo.plataformas_queda[i].q_inicio = time(NULL);
				return modelo.plataformas_queda[i].z + 5;
			}

		}
	}

	for (int i = 0; i < modelo.nmovim; i++) {
		if (modelo.protagonista[prot].x > (modelo.plataformas_movim[i].x - (float)modelo.tamanho_plat / 2) && modelo.protagonista[prot].x < (modelo.plataformas_movim[i].x + (float)modelo.tamanho_plat / 2) && modelo.protagonista[prot].y < (modelo.plataformas_movim[i].y + (float)modelo.tamanho_plat / 2) && modelo.protagonista[prot].y >(modelo.plataformas_movim[i].y - (float)modelo.tamanho_plat / 2)) {

			if (modelo.protagonista[prot].z > modelo.plataformas_movim[i].z)
				if (modelo.plataformas_movim[i].m_vert == GL_TRUE)
					modelo.protagonista[prot].sombra = modelo.plataformas_movim[i].z + 1.4;
				else
					modelo.protagonista[prot].sombra = modelo.plataformas_movim[i].z + 1.5;

			if (modelo.protagonista[prot].z - 4 < (modelo.plataformas_movim[i].z + 2) && modelo.protagonista[prot].z - 4 > (modelo.plataformas_movim[i].z - 0.5)) {
				modelo.plataformas_movim[i].protPisada = prot;
				modelo.plataformas_movim[i].m_pisada[prot] = GL_TRUE;
				modelo.protagonista[prot].incSalto = 0;

				if (modelo.protagonista[prot].modelo.GetSequence() == 8)
					modelo.protagonista[prot].modelo.SetSequence(0);
				return modelo.plataformas_movim[i].z + 5;
			}
		}
		else {
			modelo.plataformas_movim[i].m_pisada[prot] = GL_FALSE;
		}
	}

	for (int i = 0; i < modelo.nfixas; i++) {
		if (modelo.protagonista[prot].x > (modelo.plataformas_fixas[i].x - (float)modelo.tamanho_plat / 2)
			&& modelo.protagonista[prot].x < (modelo.plataformas_fixas[i].x + (float)modelo.tamanho_plat / 2)
			&& modelo.protagonista[prot].y < (modelo.plataformas_fixas[i].y + (float)modelo.tamanho_plat / 2)
			&& modelo.protagonista[prot].y >(modelo.plataformas_fixas[i].y - (float)modelo.tamanho_plat / 2)) {

			if (modelo.protagonista[prot].z > modelo.plataformas_fixas[i].z)
				modelo.protagonista[prot].sombra = modelo.plataformas_fixas[i].z + 1.5;

			if (modelo.protagonista[prot].z - 4 < (modelo.plataformas_fixas[i].z + 2) && modelo.protagonista[prot].z - 4 > (modelo.plataformas_fixas[i].z - 0.5)) {

				modelo.protagonista[prot].incSalto = 0;

				if (modelo.protagonista[prot].modelo.GetSequence() == 8)
					modelo.protagonista[prot].modelo.SetSequence(0);
				return modelo.plataformas_fixas[i].z + 5;
			}
		}
	}

	for (int i = 0; i < modelo.nBolas; i++) {
		if (modelo.protagonista[prot].x > (modelo.bolaDeFogo[i].x - modelo.bolaDeFogo[i].ballSize / 2)
			&& modelo.protagonista[prot].x < (modelo.bolaDeFogo[i].x + modelo.bolaDeFogo[i].ballSize / 2)
			&& modelo.protagonista[prot].y >(modelo.bolaDeFogo[i].y - modelo.bolaDeFogo[i].ballSize / 2)
			&& modelo.protagonista[prot].y < (modelo.bolaDeFogo[i].y + modelo.bolaDeFogo[i].ballSize / 2)
			&& modelo.protagonista[prot].z >(modelo.bolaDeFogo[i].z - modelo.bolaDeFogo[i].ballSize / 2)
			&& modelo.protagonista[prot].z < (modelo.bolaDeFogo[i].z + modelo.bolaDeFogo[i].ballSize / 2))
		{
			return -2;
		}
	}

	if (modelo.protagonista[prot].x > (modelo.bonus.x - (modelo.bonus.tamanho + 0.5) / 2.0)
		&& modelo.protagonista[prot].x < (modelo.bonus.x + (modelo.bonus.tamanho + 0.5) / 2.0)
		&& modelo.protagonista[prot].y >(modelo.bonus.y - (modelo.bonus.tamanho + 0.5) / 2.0)
		&& modelo.protagonista[prot].y < (modelo.bonus.y + (modelo.bonus.tamanho + 0.5) / 2.0)
		&& modelo.protagonista[prot].z >(modelo.bonus.z - (modelo.bonus.tamanho + 0.5) / 2.0)
		&& modelo.protagonista[prot].z < (modelo.bonus.z + (modelo.bonus.tamanho + 0.5) / 2.0) && modelo.bonus.apanhado == 0)
	{
		modelo.bonus.apanhado = 1;
		modelo.protagonista[prot].doublejump = 1;
		modelo.ativarBonus = 1;
		alSourcePlay(estadoAL_doubleJump.source);
		alSourcePause(estadoAL_main.source);
		alSourcePlay(estadoAL_invincible.source);
		glutTimerFunc(BONUSTIMER, disableDoubleJump, prot);
	}


	if (modelo.protagonista[prot].x > (modelo.plataforma_fim.x - (float)modelo.tamanho_plat / 2) && modelo.protagonista[prot].x < (modelo.plataforma_fim.x + (float)modelo.tamanho_plat / 2) && modelo.protagonista[prot].y < (modelo.plataforma_fim.y + (float)modelo.tamanho_plat / 2) && modelo.protagonista[prot].y >(modelo.plataforma_fim.y - (float)modelo.tamanho_plat / 2))

	{
		if (modelo.protagonista[prot].z - 4 < (modelo.plataforma_fim.z + 2) && modelo.protagonista[prot].z - 4 > (modelo.plataforma_fim.z - 0.5))
		{
			glViewport(0, 0, WIDTH, HEIGHT);
			glutDisplayFunc(Draw);
			Draw();
			modelo.protagonista[prot].incSalto = 0;
			if (modelo.protagonista[prot].modelo.GetSequence() == 8)
				modelo.protagonista[prot].modelo.SetSequence(0);
			modelo.activa = 0;
			modelo.protagonista[prot].x = modelo.plataforma_fim.x;
			modelo.protagonista[prot].y = modelo.plataforma_fim.y;
			modelo.protagonista[prot].angulo = 270;
			modelo.protagonista[prot].modelo.SetSequence(35);
			inicia_camera_fim(prot);
			estado.cameraAtiva = 3;

			alSourceStop(estadoAL_main.source);
			alSourcePlay(estadoAL_ending.source);

			return modelo.plataforma_fim.z + 5;
		}
	}

	return -1;
}

void highlightMenu(int x, int y) {
	GLfloat fx = ((float)x / WIDTH) - 0.5f;
	GLfloat fy = (((float)y / HEIGHT) - 0.5f)*-1;
	if (estado.menuActivo == 1) {
		for (int i = 0; i < 4; i++) {
			if (fx > menuButton[i][0][0] - 0.5 && fx < menuButton[i][2][0] + 0.5) {
				if (fy < menuButton[i][0][1] && fy > menuButton[i][2][1]) {
					menuPrincipalOpcaoSelecionada = i;
					return;
				}
			}
		}
	}
	if (estado.menuActivo == 3) {
		for (int i = 4; i < 7; i++) {
			if (fx > menuButton[i][0][0] - 0.5 && fx < menuButton[i][2][0] + 0.5) {
				if (fy < menuButton[i][0][1] && fy > menuButton[i][2][1]) {
					menuPrincipalOpcaoSelecionada = i;
					return;
				}
			}
		}
	}
	if (estado.menuActivo == 4) {
		for (int i = 7; i < 10; i++) {
			if (fx > menuButton[i][0][0] - 0.5 && fx < menuButton[i][2][0] + 0.5) {
				if (fy < menuButton[i][0][1] && fy > menuButton[i][2][1]) {
					menuPrincipalOpcaoSelecionada = i;
					return;
				}
			}
		}
	}
	menuPrincipalOpcaoSelecionada = -1;
}

void clickMenu(int button, int state, int x, int y) {
	GLfloat fx = ((float)x / WIDTH) - 0.5f;
	GLfloat fy = (((float)y / HEIGHT) - 0.5f)*-1;
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		for (int i = 0; i < 10; i++) {

			if (fx > menuButton[i][0][0] - 0.5 && fx < menuButton[i][2][0] + 0.5) {
				if (fy < menuButton[i][0][1] && fy > menuButton[i][2][1]) {
					if (estado.menuActivo == 1) {
						if (i == 0) {
							alSourceStop(estadoAL_options.source);
							estado.menuActivo = 2;
							estado.multiPlayer = 0;
							alSourceStop(estadoAL_main.source);
							Init();
							alSourcePlay(estadoAL_main.source);
							estado.teclas.x = GL_TRUE;
							glutDisplayFunc(Draw);
							Reshape(WIDTH, HEIGHT);
						}
						if (i == 1) {
							alSourceStop(estadoAL_options.source);
							estado.menuActivo = 2;
							estado.multiPlayer = 1;
							alSourceStop(estadoAL_main.source);
							Init();
							alSourcePlay(estadoAL_main.source);
							estado.teclas.x = GL_TRUE;
							glutDisplayFunc(DisplayMultiplayer);
							Reshape(WIDTH / 2, HEIGHT);
						}
						if (i == 2) {
							estado.menuActivo = 4;
							glutDisplayFunc(DrawMenu3);
						}
						if (i == 3) {
							glutLeaveMainLoop();
						}

					}
					if (estado.menuActivo == 3) {
						if (i == 4) {
							alSourceStop(estadoAL_options.source);
							if (modelo.protagonista[0].doublejump == 1 || modelo.protagonista[1].doublejump == 1) {
								alSourcePlay(estadoAL_invincible.source);
							}
							alSourcePlay(estadoAL_main.source);
							if (estado.multiPlayer == 0) { estado.menuActivo = 2;  glutDisplayFunc(Draw); }
							if (estado.multiPlayer == 1) { estado.menuActivo = 2; Reshape(WIDTH / 2, HEIGHT); glutDisplayFunc(DisplayMultiplayer); }
						}
						if (i == 5) {
							alSourceStop(estadoAL_options.source);
							alSourceStop(estadoAL_main.source);
							alSourcePlay(estadoAL_main.source);
							if (estado.multiPlayer == 0) { Init(); estado.teclas.x = GL_TRUE; estado.menuActivo = 2; Reshape(WIDTH, HEIGHT); glutDisplayFunc(Draw); }
							if (estado.multiPlayer == 1) { Init();  estado.teclas.x = GL_TRUE; estado.menuActivo = 2; Reshape(WIDTH / 2, HEIGHT); glutDisplayFunc(DisplayMultiplayer); }
						}
						if (i == 6) {
							estado.menuActivo = 1;
							glutDisplayFunc(DrawMenu);
						}
					}
					if (estado.menuActivo == 4) {
						if (i == 7) {
							if (volume < 1.0) {
								volume += 0.1;
								changeVolume();
							}
						}
						if (i == 8) {
							if (volume > 0.0) {
								volume -= 0.1;
								changeVolume();
							}
						}
						if (i == 9) {
							estado.menuActivo = 1;
							glutDisplayFunc(DrawMenu);
						}
					}
				}
			}
		}
	}
	menuPrincipalOpcaoSelecionada = -1;
}

void movimentoPlayer(int i) {

	modelo.protagonista[i].z += modelo.protagonista[i].velZ;
	if (modelo.protagonista[i].z - modelo.protagonista[i].sombra >= 20) {
		raio = 0;
	}
	else if (modelo.protagonista[i].z - modelo.protagonista[i].sombra < 20 && modelo.protagonista[i].z - modelo.protagonista[i].sombra>0) {
		raio = (float)((float)1 - ((float)((float)1 * ((float)modelo.protagonista[i].z - modelo.protagonista[i].sombra)) / 20)) * 2;
	}
	else {
		raio = 2.0;
	}

	//Para onde a camera está a apontar
	for (int c = 0; c < NCAMERAS; c++) {
		estado.camera[i][c].center.x = modelo.protagonista[i].x;
		estado.camera[i][c].center.y = modelo.protagonista[i].y;
		estado.camera[i][c].center.z = modelo.protagonista[i].z;

		if (c == 0) {
			estado.camera[i][c].center.y = modelo.protagonista[i].y - 10;
			estado.camera[i][c].eye.z = modelo.protagonista[i].z;
		}
		if (c == 1) {
			estado.camera[i][c].eye.z = modelo.protagonista[i].z;
		}
		if (c == 2) {
			estado.camera[i][c].eye.z = modelo.protagonista[i].z + 35;
		}
	}

	int estouEmMovimento = 0;

	//Movimento para a frente
	if ((estado.teclas.w == GL_TRUE&& i == 0) || (estado.teclas.i == GL_TRUE && i == 1)) {
		modelo.protagonista[i].x += float(modelo.protagonista[i].velocidade*cos((double)RAD(modelo.protagonista[i].angulo)));
		modelo.protagonista[i].y += float(modelo.protagonista[i].velocidade*sin((double)RAD(modelo.protagonista[i].angulo)));
		for (int c = 0; c < NCAMERAS; c++) {
			estado.camera[i][c].eye.x += float(modelo.protagonista[i].velocidade*cos((double)RAD(modelo.protagonista[i].angulo)));
			estado.camera[i][c].eye.y += float(modelo.protagonista[i].velocidade*sin((double)RAD(modelo.protagonista[i].angulo)));
		}
		estouEmMovimento++;
	}

	//Movimento para a direita
	if ((estado.teclas.a == GL_TRUE && i == 0) || (estado.teclas.j == GL_TRUE && i == 1)) {
		modelo.protagonista[i].angulo += 5.0;
		for (int c = 0; c < NCAMERAS; c++) {
			estado.camera[i][c].angulo -= 5.0;
		}
	}

	//Movimento para a trás
	if ((estado.teclas.s == GL_TRUE && i == 0) || (estado.teclas.k == GL_TRUE && i == 1)) {
		modelo.protagonista[i].x -= float(modelo.protagonista[i].velocidade*cos((double)RAD(modelo.protagonista[i].angulo)));
		modelo.protagonista[i].y -= float(modelo.protagonista[i].velocidade*sin((double)RAD(modelo.protagonista[i].angulo)));
		for (int c = 0; c < NCAMERAS; c++) {
			estado.camera[i][c].eye.x -= float(modelo.protagonista[i].velocidade*cos((double)RAD(modelo.protagonista[i].angulo)));
			estado.camera[i][c].eye.y -= float(modelo.protagonista[i].velocidade*sin((double)RAD(modelo.protagonista[i].angulo)));
		}
		estouEmMovimento++;
	}

	//Movimento para a esquerda
	if ((estado.teclas.d == GL_TRUE && i == 0) || (estado.teclas.l == GL_TRUE && i == 1)) {

		modelo.protagonista[i].angulo -= 5.0;
		for (int c = 0; c < NCAMERAS; c++) {
			estado.camera[i][c].angulo += 5.0;
		}
	}

	if (estouEmMovimento > 0) {
		if (modelo.protagonista[i].modelo.GetSequence() != 3 && modelo.protagonista[i].modelo.GetSequence() != 8 && modelo.protagonista[i].modelo.GetSequence() != 15)
			modelo.protagonista[i].modelo.SetSequence(3);
	}
	else {
		if (modelo.protagonista[i].modelo.GetSequence() != 0 && modelo.protagonista[i].modelo.GetSequence() != 8 && modelo.protagonista[i].modelo.GetSequence() != 15)
			modelo.protagonista[i].modelo.SetSequence(0);
	}



	// Tecla de salto
	if ((estado.teclas.r == GL_TRUE && i == 0) || (estado.teclas.p == GL_TRUE && i == 1)) {
		if (modelo.protagonista[i].onground == GL_TRUE) {
			modelo.protagonista[i].velZ = 1.0;
			modelo.protagonista[i].z += 1.0;
			modelo.protagonista[i].incSalto += 1.0;
			alSourcePlay(estadoAL_jump.source);
			modelo.protagonista[i].modelo.SetSequence(8);
		}
		else {
			modelo.protagonista[i].incSalto += 1.0;
		}

		if (modelo.protagonista[i].doublejump == 1 && modelo.protagonista[i].jumps == 1 && modelo.protagonista[i].onground == GL_FALSE) {
			modelo.protagonista[i].incSalto = 0;
			modelo.protagonista[i].velZ = 1.0;
			modelo.protagonista[i].z += 1.0;
			modelo.protagonista[i].incSalto += 1.0;
			alSourcePlay(estadoAL_jump.source);
			modelo.protagonista[i].modelo.SetSequence(8);
			modelo.protagonista[i].jumps += 1;
		}


	}

	//Se não estiver em colisão com uma das plataformas aplica a gravidade
	int resTesta = testaColisaoTodos(i);
	if (resTesta == -1) {
		modelo.protagonista[i].z -= GRAVIDADE;
		modelo.protagonista[i].onground = GL_FALSE;
	}
	else if (resTesta == -2) {
		alSourceStop(estadoAL_main.source);
		PlaySound(TEXT("death"), NULL, SND_SYNC);
		alSourcePlay(estadoAL_main.source);
		Init();
	}
	else {

		if (modelo.protagonista[i].onground == GL_FALSE)
			modelo.protagonista[i].z = resTesta;
		modelo.protagonista[i].onground = GL_TRUE;
		modelo.protagonista[i].jumps = 0;
	}

	if (modelo.protagonista[i].incSalto >= modelo.protagonista[i].limSalto) {
		modelo.protagonista[i].velZ = 0.0;
		modelo.protagonista[i].jumps == 0;
	}

	//Se estiver no ar e a tecla de salta deixa de ser pressionada a velocidade de salto é 0
	if ((estado.teclas.r == GL_FALSE && modelo.protagonista[i].onground == GL_FALSE && i == 0) || (estado.teclas.p == GL_FALSE && modelo.protagonista[i].onground == GL_FALSE && i == 1)) {
		if (modelo.protagonista[i].jumps == 0)
			modelo.protagonista[i].jumps += 1;
		modelo.protagonista[i].velZ = 0;
	}
}


void TimerGame(int mode) {

	//LAVA AND BALL TEXTURE ANIMATION
	lavaX += 0.0005f;
	rotate += 5.5f;
	/////////////////////////////////

	if (modelo.activa) {

		//Verifica as plataformas que caiem passando uns segundos e caso o tempo suficiente já tenha passado
		//Movimenta-as
		for (int q = 0; q < modelo.nqueda; q++) {
			time_t presente = time(NULL);
			if (modelo.plataformas_queda[q].q_pisada == GL_TRUE) {

				if (difftime(presente, modelo.plataformas_queda[q].q_inicio) > 1) {
					modelo.plataformas_queda[q].z -= GRAVIDADE;
				}
			}
		}

		// Movimenta bolas de fogo
		for (int bola = 0; bola < modelo.nBolas; bola++) {
			if (modelo.bolaDeFogo[bola].z >= 20.0)
				modelo.bolaDeFogo[bola].dir -= 0.05;

			if (modelo.bolaDeFogo[bola].z <= 25.0)
				modelo.bolaDeFogo[bola].dir += 0.05;

			modelo.bolaDeFogo[bola].z += modelo.bolaDeFogo[bola].vel * modelo.bolaDeFogo[bola].dir;
		}


		//Movimenta as plataformas que são suposto fazer isso
		for (int m = 0; m < modelo.nmovim; m++) {
			if (modelo.plataformas_movim[m].m_vert == GL_FALSE) {
				if (modelo.plataformas_movim[m].x > modelo.plataformas_movim[m].m_max || modelo.plataformas_movim[m].x < modelo.plataformas_movim[m].m_min) {
					modelo.plataformas_movim[m].m_dir *= -1;
				}
				modelo.plataformas_movim[m].x += modelo.plataformas_movim[m].m_dir;
				if (modelo.plataformas_movim[m].m_pisada[0] == GL_TRUE) {
					modelo.protagonista[0].x += modelo.plataformas_movim[m].m_dir;
					for (int c = 0; c < NCAMERAS; c++) {
						estado.camera[0][c].center.x += modelo.plataformas_movim[m].m_dir;
						estado.camera[0][c].eye.x += modelo.plataformas_movim[m].m_dir;
					}
				}
				if (modelo.plataformas_movim[m].m_pisada[1] == GL_TRUE) {
					modelo.protagonista[1].x += modelo.plataformas_movim[m].m_dir;
					for (int c = 0; c < NCAMERAS; c++) {
						estado.camera[1][c].center.x += modelo.plataformas_movim[m].m_dir;
						estado.camera[1][c].eye.x += modelo.plataformas_movim[m].m_dir;
					}
				}
			}

			if (modelo.plataformas_movim[m].m_vert == GL_TRUE) {
				if (modelo.plataformas_movim[m].z > modelo.plataformas_movim[m].m_max || modelo.plataformas_movim[m].z < modelo.plataformas_movim[m].m_min) {
					modelo.plataformas_movim[m].m_dir *= -1;
				}
				modelo.plataformas_movim[m].z += modelo.plataformas_movim[m].m_dir;
				if (modelo.plataformas_movim[m].m_pisada[0] == GL_TRUE) {
					modelo.protagonista[0].z += modelo.plataformas_movim[m].m_dir;
					for (int c = 0; c < NCAMERAS; c++) {
						estado.camera[0][c].center.z += modelo.plataformas_movim[m].m_dir;
						estado.camera[0][c].eye.z += modelo.plataformas_movim[m].m_dir;
					}
				}
				if (modelo.plataformas_movim[m].m_pisada[1] == GL_TRUE) {
					modelo.protagonista[1].z += modelo.plataformas_movim[m].m_dir;
					for (int c = 0; c < NCAMERAS; c++) {
						estado.camera[1][c].center.z += modelo.plataformas_movim[m].m_dir;
						estado.camera[1][c].eye.z += modelo.plataformas_movim[m].m_dir;
					}
				}
			}
		}

		movimentoPlayer(0);
		if (estado.multiPlayer == 1) {
			movimentoPlayer(1);
		}

		if (estado.multiPlayer == 0 && modelo.protagonista[0].z < -15) {
			modelo.protagonista[0].modelo.SetSequence(15);
			inicia_modelo();
			alSourceStop(estadoAL_main.source);
			PlaySound(TEXT("death"), NULL, SND_SYNC);
			alSourcePlay(estadoAL_main.source);
			shutdownLight();
			Init();
		}

		if (estado.multiPlayer == 1 && modelo.protagonista[0].z < -15) {
			alSourceStop(estadoAL_main.source);
			PlaySound(TEXT("death"), NULL, SND_SYNC);
			glViewport(0, 0, WIDTH, HEIGHT);
			Reshape(WIDTH, HEIGHT);
			glutDisplayFunc(Draw);
			Draw();
			modelo.activa = 0;
			modelo.protagonista[1].x = modelo.plataforma_fim.x;
			modelo.protagonista[1].y = modelo.plataforma_fim.y;
			modelo.protagonista[1].z = modelo.plataforma_fim.z + 5;
			modelo.protagonista[1].angulo = 270;
			modelo.protagonista[1].modelo.SetSequence(35);
			inicia_camera_fim(1);
			estado.cameraAtiva = 3;
			alSourceStop(estadoAL_main.source);
			alSourcePlay(estadoAL_ending.source);
		}

		if (estado.multiPlayer == 1 && modelo.protagonista[1].z < -15) {
			alSourceStop(estadoAL_main.source);
			PlaySound(TEXT("death"), NULL, SND_SYNC);
			glViewport(0, 0, WIDTH, HEIGHT);
			Reshape(WIDTH, HEIGHT);
			glutDisplayFunc(Draw);
			Draw();
			modelo.activa = 0;
			modelo.protagonista[0].x = modelo.plataforma_fim.x;
			modelo.protagonista[0].y = modelo.plataforma_fim.y;
			modelo.protagonista[0].z = modelo.plataforma_fim.z + 5;
			modelo.protagonista[0].angulo = 270;
			modelo.protagonista[0].modelo.SetSequence(35);
			inicia_camera_fim(0);
			estado.cameraAtiva = 3;
			alSourceStop(estadoAL_main.source);
			alSourcePlay(estadoAL_ending.source);
		}

	}
}

void startMenu(int i) {
	estado.menuActivo = 1;
	alSourceStop(estadoAL_intro.source);
	Reshape(WIDTH, HEIGHT);
	alSourcePlay(estadoAL_options.source);
	glutDisplayFunc(DrawMenu);
}


void TimerMenu() {
	glutPassiveMotionFunc(highlightMenu);
	glutMouseFunc(clickMenu);
}

/*******************************
***   callbacks timer/idle   ***
*******************************/


void Timer(int value)
{
	glutTimerFunc(estado.delayMovimento, Timer, 0);
	TimerMenu();
	TimerGame(0);
	//Sair em caso de o jogo estar parado ou menu estar activo
	if (modelo.parado)
		return;
	// redesenhar o ecra 
	glutPostRedisplay();
}


void imprime_ajuda(void)
{
	printf("\n\nPLATAFORMS\n");
	printf("h,H - Ajuda \n");
	printf("w,W - Move forward\n");
	printf("a,A - Move left\n");
	printf("d,D - Move right\n");
	printf("s,S - Move backwards\n");
	printf("j,J,SpaceBar - Jump\n");
	printf("r,R - Restart level\n");
	printf("ESC - Exit\n");
}

/*******************************
***  callbacks de teclado    ***
*******************************/

// Callback para interaccao via teclado (carregar na tecla)

void Key(unsigned char key, int x, int y)
{
	switch (key) {

	case ESCAPE: //Escape
		if (estado.menuActivo == 0 || estado.menuActivo == 2) {
			if (estado.cameraAtiva == 3) {
				modelo.activa = 1;
				inicia_modelo();
				estado.menuActivo = 1;
				glEnable(GL_SCISSOR_TEST);
				glViewport(0, 0, WIDTH, HEIGHT);
				glScissor(0, 0, WIDTH, HEIGHT);
				glLoadIdentity();
				alSourceStop(estadoAL_ending.source);
				Reshape(WIDTH, HEIGHT);
				alSourcePlay(estadoAL_options.source);
				glutDisplayFunc(DrawMenu);
			}
			else {
				estado.menuActivo = 3;
				modelo.activa = 1;
				glEnable(GL_SCISSOR_TEST);
				glViewport(0, 0, WIDTH, HEIGHT);
				glScissor(0, 0, WIDTH, HEIGHT);
				glLoadIdentity();
				alSourcePause(estadoAL_main.source);
				alSourcePause(estadoAL_invincible.source);
				Reshape(WIDTH, HEIGHT);
				alSourcePlay(estadoAL_options.source);
				glutDisplayFunc(DrawMenu2);
			}
		}
		break;
	case 'h':
	case 'H':
		imprime_ajuda();
		break;
	case 'W':
	case 'w':
		estado.teclas.w = GL_TRUE; break;
	case 'A':
	case 'a':
		estado.teclas.a = GL_TRUE; break;
	case 'S':
	case 's':
		estado.teclas.s = GL_TRUE; break;
	case 'D':
	case 'd':
		estado.teclas.d = GL_TRUE; break;
	case 'r':
	case 'R': estado.teclas.r = GL_TRUE; break;
	case 'z':
	case 'Z':
		if (estado.cameraAtiva != 3) {
			estado.cameraAtiva += 1;
			if (estado.cameraAtiva > 2) {
				estado.cameraAtiva = 0;
			}
		}
		break;
	case 'x':
	case 'X':estado.teclas.x = GL_TRUE;
		if (estado.multiPlayer == 0) {
			inicia_modelo();
			inicia_camera_fps();
			inicia_camera_top();
			inicia_camera_tps();
			modelo.activa = 1;
			estado.cameraAtiva = 1;
			alSourceStop(estadoAL_main.source);		// Kills main_theme sound
			alSourceStop(estadoAL_ending.source);
			Init();
			alSourcePlay(estadoAL_main.source);
		}
		else {
			inicia_modelo();
			inicia_camera_fps();
			inicia_camera_top();
			inicia_camera_tps();
			modelo.activa = 1;
			estado.cameraAtiva = 1;
			alSourceStop(estadoAL_main.source);		// Kills main_theme sound
			alSourceStop(estadoAL_ending.source);
			Init();
			alSourcePlay(estadoAL_main.source);
			Reshape(WIDTH / 2, HEIGHT);
			glutDisplayFunc(DisplayMultiplayer);

		}
		break;
	case 'j':
	case 'J':
		estado.teclas.j = GL_TRUE; break;
	case 'i':
	case 'I':
		estado.teclas.i = GL_TRUE; break;
	case 'k':
	case 'K':
		estado.teclas.k = GL_TRUE; break;
	case 'p':
	case 'P':
		estado.teclas.p = GL_TRUE; break;
	case 'l':
	case 'L':
		estado.teclas.l = GL_TRUE; break;
	}

	if (estado.debug)
		printf("Carregou na tecla %c\n", key);

}

// Callback para interaccao via teclado (largar a tecla)

void KeyUp(unsigned char key, int x, int y)
{
	switch (key) {
	case 'W':
	case 'w': estado.teclas.w = GL_FALSE; break;
	case 'A':
	case 'a': estado.teclas.a = GL_FALSE; break;
	case 'S':
	case 's': estado.teclas.s = GL_FALSE; break;
	case 'D':
	case 'd': estado.teclas.d = GL_FALSE; break;
	case 'r':
	case 'R': estado.teclas.r = GL_FALSE; break;
	case 'I':
	case 'i': estado.teclas.i = GL_FALSE; break;
	case 'J':
	case 'j': estado.teclas.j = GL_FALSE; break;
	case 'K':
	case 'k': estado.teclas.k = GL_FALSE; break;
	case 'L':
	case 'l': estado.teclas.l = GL_FALSE; break;
	case 'p':
	case 'P': estado.teclas.p = GL_FALSE; break;
	case 'x':
	case 'X':estado.teclas.x = GL_FALSE; break;
	}

	if (estado.debug)
		printf("Largou a tecla %c\n", key);
}

// Callback para interaccao via teclas especiais  (carregar na tecla)

void SpecialKey(int key, int x, int y)
{
	// ... accoes sobre outras teclas especiais ... 
	//    GLUT_KEY_F1 ... GLUT_KEY_F12
	//    GLUT_KEY_UP
	//    GLUT_KEY_DOWN
	//    GLUT_KEY_LEFT
	//    GLUT_KEY_RIGHT
	//    GLUT_KEY_PAGE_UP
	//    GLUT_KEY_PAGE_DOWN
	//    GLUT_KEY_HOME
	//    GLUT_KEY_END
	//    GLUT_KEY_INSERT

	/*
	switch (key) {

	redesenhar o ecra
	glutPostRedisplay();
	}
	*/

	if (estado.debug)
		printf("Carregou na tecla especial %d\n", key);
}

// Callback para interaccao via teclas especiais (largar na tecla)

void SpecialKeyUp(int key, int x, int y)
{
	/*switch (key) {
	}*/
	if (estado.debug)
		printf("Largou a tecla especial %d\n", key);

}

int main(int argc, char **argv)
{
	alutInit(0, NULL);		// Sound - Alut initializer
	volume = 0.5f;
	char str[] = " makefile MAKEFILE Makefile ";
	estado.doubleBuffer = 1;
	estado.menuActivo = 4;
	player = 0;
	glutInit(&argc, argv);
	glutGameModeString("1920x1080:32@60"); // 1º teste
	if (glutGameModeGet(GLUT_GAME_MODE_POSSIBLE)) {
		WIDTH = 1920;
		HEIGHT = 1080;
		glutEnterGameMode();
	}
	else {
		glutGameModeString("1366x768:32@60");
		if (glutGameModeGet(GLUT_GAME_MODE_POSSIBLE))
		{
			WIDTH = 1366;
			HEIGHT = 768;
			glutEnterGameMode();
		}
		else {
			glutGameModeString("1024x768:32@60"); // 1º teste
			if (glutGameModeGet(GLUT_GAME_MODE_POSSIBLE)) {
				WIDTH = 1024;
				HEIGHT = 768;
				glutEnterGameMode();
			}
			else
			{
				glutGameModeString("800x600:32@60"); // 2º teste
				if (glutGameModeGet(GLUT_GAME_MODE_POSSIBLE))
				{
					WIDTH = 800;
					HEIGHT = 600;
					glutEnterGameMode();
				}
				else // Cria Janela Normal
				{
					glutInitWindowPosition(10, 10);
					WIDTH = 800;
					HEIGHT = 600;
					glutInitWindowSize(800, 600);
					glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGBA | GLUT_DEPTH | GLUT_ALPHA);
					if (glutCreateWindow("Projecto Sgrai - Jogo de plataformas 3D") == GL_FALSE)
						exit(1);

				}
			}
		}
	}
	initAllAudio();
	Init();
	modelo.activa = 1;

	imprime_ajuda();

	// Registar callbacks do GLUT

	// callbacks de janelas/desenho
	glutReshapeFunc(Reshape);
	alSourcePlay(estadoAL_intro.source);
	glutDisplayFunc(DrawIntro);
	glutTimerFunc(2000, startMenu, 0);

	// Callbacks de teclado
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	// callbacks timer/idle
	glutTimerFunc(estado.delayMovimento, Timer, 0);

	skybox = new SKYBOX();
	if (skybox->Initialize())
	{
		// Início da aplicação
		glutMainLoop();

		// Destruição da skybox
		skybox->Finalize();
		delete skybox;

		return 0;
	}
	return 1;
}
